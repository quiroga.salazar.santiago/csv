package org.example;

import java.io.FileWriter;

public class CreateCsv {

    public static void main(String[] args) {
        StringBuilder builder = new StringBuilder();

        builder.append("Name").append(",").append("Last Name").append(",").append("Age").append("\n");

        builder.append("Santiago").append(",").append("Quiroga").append(",").append(18).append("\n");
        builder.append("Federico").append(",").append("Montaño").append(",").append(22).append("\n");

        try(FileWriter writer = new FileWriter("CsvExample.csv")) {
            writer.write(builder.toString());
            System.out.println("Csv file created.");

        }

        catch(Exception ignored){
        }
    }
}

